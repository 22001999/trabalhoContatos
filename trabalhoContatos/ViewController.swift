//
//  ViewController.swift
//  trabalhoContatos
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome: "Contato 1", numero: "31 90904567", email: "pessoa1@gmail.com", endereco: "Rua 1, Bairro 1"))
        listaDeContatos.append(Contato(nome: "Contato 2", numero: "31 90907635", email: "pessoa2@gmail.com", endereco: "Rua 2, Bairro 2"))
        listaDeContatos.append(Contato(nome: "Contato 3", numero: "31 90900386", email: "pessoa3@gmail.com", endereco: "Rua 3, Bairro 3"))
        listaDeContatos.append(Contato(nome: "Contato 4", numero: "31 90901234", email: "pessoa4@gmail.com", endereco: "Rua 4, Bairro 4"))
        listaDeContatos.append(Contato(nome: "Contato 5", numero: "31 90908643", email: "pessoa5@gmail.com", endereco: "Rua 5, Bairro 5"))
    }


}

